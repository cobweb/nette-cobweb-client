<?php
/**
 * This file is part of the "Nette Cobweb Client"
 *
 * Copyright (c) 2014 Ing. Jan Svantner (http://www.janci.net)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace Tests\JPSoftware\Cobweb\RestClient;

use JPSoftware\Cobweb\RestClient\DefaultRestClient;
use Tester\Assert;
use Tester\TestCase;

$container = require_once __DIR__ . '/../../../../bootstrap.php';

class DefaultRestClientTest extends TestCase
{
    /** @var DefaultRestClient */
    private $restClient;

    public function setup()
    {
        $this->restClient = $rest = new DefaultRestClient();
    }

    public function testSendLog()
    {
        $this->restClient->setProjectToken("tester");
        $this->restClient->setServerUri("http://api.cobweb.janci.net/1.0.0");
        $check = $this->restClient->log("Hello Kitty", "debug", new \DateTime());

        Assert::true($check);
    }
}

id(new DefaultRestClientTest($container))->run();
