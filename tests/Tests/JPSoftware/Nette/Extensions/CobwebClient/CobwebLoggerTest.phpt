<?php
/**
 * This file is part of the "Nette Cobweb Client".
 *
 * Copyright (c) 2014 Ing. Jan Svantner (http://www.janci.net)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace Tests\JPSoftware\Nette\Extensions\CobwebClient;

use JPSoftware\Cobweb\RestClient\DummyRestClient;
use JPSoftware\Nette\Extensions\CobwebClient\CobwebLogger;
use Nette\DI\Container;
use Tester\Assert;
use Tester\TestCase;
use Tracy\Logger;

$container = require_once __DIR__ . '/../../../../../bootstrap.php';

class CobwebLoggerTest extends TestCase
{
    /**
     * @var Container
     */
    private $container;

    /** @var CobwebLogger */
    private $logger;

    /** @var DummyRestClient */
    private $restClient;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function setup()
    {
        $this->restClient = $rest = new DummyRestClient();

        $directory = TEST_DIR . DIRECTORY_SEPARATOR . 'logs';
        $this->logger = new CobwebLogger($rest, $directory);
    }

    public function testInfo()
    {
        $this->logger->log("INFO", Logger::INFO);
        Assert::false($this->restClient->getLastLog());
    }

    public function testWarning()
    {
        $this->logger->log("WARNING", Logger::WARNING);
        $log = $this->restClient->getLastLog();
        Assert::same('WARNING', $log['message']);
        Assert::same('warning', $log['state']);
    }

    public function testError()
    {
        $this->logger->log("ERROR", Logger::ERROR);
        $log = $this->restClient->getLastLog();
        Assert::same('ERROR', $log['message']);
        Assert::same('error', $log['state']);
    }

    public function testException()
    {
        $e = new \Exception("EXCEPTION", 0);
        $this->logger->log($e, Logger::EXCEPTION);

        $log = $this->restClient->getLastLog();

        $output = $e = '[Exception]' . "\n" . $e->getMessage() . "\n" . $e->getTraceAsString();;
        Assert::same($output, $log['message']);
        Assert::same('exception', $log['state']);
    }

    public function testStripDateTime()
    {
        $this->logger->log("[2014-09-08 10-33-59] ERROR", Logger::ERROR);
        $log = $this->restClient->getLastLog();
        Assert::same('ERROR', $log['message']);
        Assert::same('error', $log['state']);
    }

    public function testStripDateTime_NewLine()
    {
        $this->logger->log("[2014-09-08 10-33-59] ERROR\nNEW LINE", Logger::ERROR);
        $log = $this->restClient->getLastLog();
        Assert::same("ERROR\nNEW LINE", $log['message']);
        Assert::same('error', $log['state']);
    }
}

id(new CobwebLoggerTest($container))->run();
