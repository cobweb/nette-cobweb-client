<?php
/**
 * This file is part of the "Nette Cobweb Client".
 *
 * Copyright (c) 2014 Ing. Jan Svantner (http://www.janci.net)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

require_once __DIR__ . '/../../../../../../bootstrap.php';

$testScript = __DIR__ . DIRECTORY_SEPARATOR . "realSimulation.php";

if (!defined("PHP_BINARY")) {
    if (defined("PHP_BIN_DIR")) {
        define("PHP_BINARY", PHP_BIN_DIR);
    } else {
        define("PHP_BINARY", "php");
    }
}

exec(PHP_BINARY . " " . $testScript, $output);

\Tester\Assert::true(end($output) !== false);
\Tester\Assert::same('OK!', end($output));
