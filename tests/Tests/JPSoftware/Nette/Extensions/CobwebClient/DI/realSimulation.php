<?php
/**
 * This file is part of the "Nette Cobweb Client".
 *
 * Copyright (c) 2014 Ing. Jan Svantner (http://www.janci.net)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

use JPSoftware\Cobweb\RestClient\DummyRestClient;
use Nette\Application\Application;
use Nette\Configurator;
use Nette\DI\Container;
use Tester\Assert;
use Tester\Helpers;
use Tracy\Debugger;

require_once __DIR__ . '/../../../../../../bootstrap.php';

/**
 * @param string $configFile
 * @return Container
 */
function createContainer($configFile)
{
    Helpers::purge(TEMP_DIR.'-1');

    $config = new Configurator();
    $config->enableDebugger(TEST_DIR . DIRECTORY_SEPARATOR . 'logs');
    $config->setTempDirectory(TEMP_DIR.'-1');
    $config->addParameters(array('container' => array('class' => 'SystemContainer_' . md5($configFile))));
    $config->addConfig(TEST_DIR . '/test-reset.neon');
    $config->addConfig(__DIR__ . '/configs/' . $configFile . '.neon');

    return $config->createContainer();
}

Debugger::$productionMode = FALSE;
$container = createContainer('load-extension');

/** @var \Tracy\Logger  $logger */
$logger = Debugger::getLogger();
Debugger::enable();

/** @var DummyRestClient $cobwebRestClient */
$cobwebRestClient = $container->getByType("JPSoftware\\Cobweb\\RestClient\\DummyRestClient");


/** @var Application $application */
$application = $container->getService("application");

register_shutdown_function(function() use ($cobwebRestClient) {
    $log = $cobwebRestClient->getLastLog();
    Assert::notSame(false, $log);
    Assert::same('exception', $log['state']);
    ob_get_clean();
    echo "\n";
    echo 'OK!';
});

ob_start();
$application->catchExceptions = false;
$application->run();

