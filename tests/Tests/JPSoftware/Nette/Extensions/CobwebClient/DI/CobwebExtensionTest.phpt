<?php
/**
 * This file is part of the "Nette Cobweb Client".
 *
 * Copyright (c) 2014 Ing. Jan Svantner (http://www.janci.net)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace Tests\JPSoftware\Nette\Extensions\CobwebClient\DI;

use JPSoftware\Cobweb\RestClient\DummyRestClient;
use Nette\Configurator;
use Nette\DI\Container;
use Tester\Assert;
use Tester\Helpers;
use Tester\TestCase;
use Tracy\Debugger;

require_once __DIR__ . '/../../../../../../bootstrap.php';

class CobwebExtensionTest extends TestCase
{

    /**
     * @param string $configFile
     * @return Container
     */
    public function createContainer($configFile)
    {
        Helpers::purge(TEMP_DIR.'-1');

        $config = new Configurator();
        $config->enableDebugger(TEST_DIR . DIRECTORY_SEPARATOR . 'logs');
        $config->setTempDirectory(TEMP_DIR.'-1');
        $config->addParameters(array('container' => array('class' => 'SystemContainer_' . md5($configFile))));
        $config->addConfig(TEST_DIR . '/test-reset.neon');
        $config->addConfig(__DIR__ . '/configs/' . $configFile . '.neon');

        return $config->createContainer();
    }



    public function testFunctionality()
    {
        $container = $this->createContainer('load-extension');
        $logger = Debugger::getLogger();

        /** @var DummyRestClient $restClient */
        $restClient = $container->getByType('JPSoftware\Cobweb\RestClient\ICobwebRestClient');

        Assert::type('JPSoftware\Nette\Extensions\CobwebClient\CobwebLogger', $logger);
        Assert::type('JPSoftware\Cobweb\RestClient\DummyRestClient', $restClient);
        Assert::same('file://' . TEMP_DIR . '-1/tmp', $restClient->getUri());
        Assert::same('checkTests', $restClient->getProjectToken());
    }
}

id(new CobwebExtensionTest())->run();
