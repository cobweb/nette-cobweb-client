<?php
/**
 * This file is part of the "Nette Cobweb Client".
 *
 * Copyright (c) 2014 Ing. Jan Svantner (http://www.janci.net)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

require __DIR__ . '/../vendor/autoload.php';

if ((!$loader = @include __DIR__ . '/../vendor/autoload.php') && (!$loader = @include __DIR__ . '/../../../../autoload.php')) {
    echo 'Install Nette Tester using `composer update --dev`';
    exit(1);
}

$loader->add('Tests\\JPSoftware\\Nette\\Extensions\\CobwebClient', __DIR__ );
$loader->add('Tests\\JPSoftware\\Cobweb\\RestClient', __DIR__ );

Tester\Environment::setup();

define('TEMP_DIR', __DIR__ . '/tmp/_' . (isset($_SERVER['argv']) ? md5(serialize($_SERVER['argv'])) : getmypid()));
define('TEST_DIR', __DIR__);

@mkdir(__DIR__ . '/tmp');
\Tester\Helpers::purge(TEMP_DIR);
\Tester\Helpers::purge(TEST_DIR . DIRECTORY_SEPARATOR . 'logs');

/**
 * @param $val
 * @return \Tester\TestCase
 */
function id($val) {
    return $val;
}

$configurator = new Nette\Configurator;
$configurator->setDebugMode(TRUE);
$configurator->setTempDirectory(TEMP_DIR);
//$configurator->enableDebugger(TEST_DIR . DIRECTORY_SEPARATOR . 'logs');

$configurator->addConfig(__DIR__ . '/test-reset.neon');
$configurator->addConfig(__DIR__ . '/test.neon');
return $configurator->createContainer();
