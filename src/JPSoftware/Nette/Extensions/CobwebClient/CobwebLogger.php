<?php
/**
 * This file is part of the "Nette Cobweb Client".
 *
 * Copyright (c) 2014 Ing. Jan Svantner (http://www.janci.net)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace JPSoftware\Nette\Extensions\CobwebClient;

use JPSoftware\Cobweb\RestClient\ICobwebRestClient;
use ReflectionClass;
use Tracy\BlueScreen;
use Tracy\Logger;

class CobwebLogger extends Logger
{
    /**
     * @var ICobwebRestClient
     */
    private $cobwebRestClient;

    /**
     * @param ICobwebRestClient $cobwebRestClient
     * @param $directory
     * @param null $email
     * @param BlueScreen $blueScreen
     */
    public function __construct(ICobwebRestClient $cobwebRestClient, $directory, $email = NULL, BlueScreen $blueScreen = NULL)
    {
        $this->cobwebRestClient = $cobwebRestClient;

        $loggerReflection = new ReflectionClass('Tracy\Logger');
        if ($loggerReflection->hasMethod('__construct')) {
            parent::__construct($directory, $email, $blueScreen);
        }

        $this->directory = & $directory; // back compatiblity
        $this->email = & $email;
    }

    /**
     * @inheritdoc
     */
    public function log($message, $priority = NULL)
    {
        $file = parent::log($message, $priority);

        $allowLevels = array(
            self::CRITICAL => true,
            self::ERROR => true,
            self::EXCEPTION => true,
            self::WARNING => true,
        );

        try {
            if (isset($allowLevels[$priority])) {
                if (is_array($message)) {
                    $message = implode(' ', $message);
                }

                if ($message instanceof \Exception) {
                    $className = get_class($message);
                    $message = '[' . $className . ']' . "\n" . $message->getMessage() . "\n" . $message->getTraceAsString();
                }

                $message = preg_replace("~^(\\[[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}-[0-9]{2}-[0-9]{2}\\] )([\\s\\S]*)$~", "$2", $message);
                if (is_string($file)) {
                    $this->cobwebRestClient->logFile($file, $message, $priority, new \DateTime());
                } else {
                    $this->cobwebRestClient->log($message, $priority, new \DateTime());
                }
            }
        } catch (\Exception $e) {
            return parent::log($e, 'exception');
        }

        return $file;
    }
}
