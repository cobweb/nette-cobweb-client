<?php
/**
 * This file is part of the "Nette Cobweb Client".
 *
 * Copyright (c) 2014 Filip Vozár <filip.vozar@gmail.com>
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace JPSoftware\Nette\Extensions\CobwebClient\DI;

use Nette;
use Nette\DI\CompilerExtension;

/**
 * Class CobwebExtension
 *
 * @package JPSoftware\Nette\Extensions\CobwebClient\DI
 */
class CobwebExtension extends CompilerExtension
{
    /**
     * @var array
     */
    public $defaults = array(
        'uri' => 'http://localhost/',
        'token' => 'unknown'
    );

    public function loadConfiguration()
    {
        $builder = $this->getContainerBuilder();
        $config = $this->getConfig($this->defaults);

        $builder->addDefinition($this->prefix('cobweb.restClient'))
            ->setClass('JPSoftware\Cobweb\RestClient\DefaultRestClient')
            ->addSetup('setProjectToken', array($config['token']))
            ->addSetup('setServerUri', array($config['uri']))
            ->setInject(false)
            ->setAutowired(true);

    }

    public function beforeCompile()
    {
        $builder = $this->getContainerBuilder();
        $builder->prepareClassList();

        try {
            $builder->getByType('JPSoftware\Cobweb\RestClient\ICobwebRestClient');
        } catch (Nette\DI\ServiceCreationException $e) {
            $config = $this->getConfig($this->defaults);
            $builder->removeDefinition($this->prefix('cobweb.restClient'));
            $builder->prepareClassList();

            $definitionName = $builder->getByType('JPSoftware\Cobweb\RestClient\ICobwebRestClient');
            $builder->getDefinition($definitionName)
                ->addSetup('setProjectToken', array($config['token']))
                ->addSetup('setServerUri', array($config['uri']));
        }

    }


    public function afterCompile(Nette\PhpGenerator\ClassType $class)
    {
        $methods = $class->getMethods();
        $initialize = $methods['initialize'];
        $initialize->addBody('
            $restClient = $this->getByType(\'JPSoftware\Cobweb\RestClient\ICobwebRestClient\');

            $logDirectory = Tracy\Debugger::$logDirectory;
            $email = Tracy\Debugger::$email;
            $blueScreen = Tracy\Debugger::getBlueScreen();

            $logger = new JPSoftware\Nette\Extensions\CobwebClient\CobwebLogger($restClient, $logDirectory, $email, $blueScreen);
            Tracy\Debugger::setLogger($logger);');
    }
}
