<?php
/**
 * This file is part of the "Nette Cobweb Client".
 *
 * Copyright (c) 2014 Ing. Jan Svantner (http://www.janci.net)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace JPSoftware\Cobweb\RestClient;

class DummyRestClient implements ICobwebRestClient
{

    private $uri;

    private $projectToken;

    private $logs = array();

    public function setServerUri($uri = "http://localhost/")
    {
        $this->uri = $uri;
    }

    public function setProjectToken($projectToken = "unknown")
    {
        $this->projectToken = $projectToken;
    }

    public function log($message, $state, \DateTime $errorDateTime)
    {
        $this->logs[] = array(
            'message' => $message,
            'state' => $state,
            'created' => $errorDateTime,
            'fileName' => null
        );
    }

    public function logFile($fileName, $message, $state, \DateTime $errorDateTime)
    {
        $this->logs[] = array(
            'fileName' => $fileName,
            'message' => $message,
            'state' => $state,
            'created' => $errorDateTime
        );
    }

    /**
     * @return string
     */
    public function getProjectToken()
    {
        return $this->projectToken;
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    public function getLastLog()
    {
        return end($this->logs);
    }
}
