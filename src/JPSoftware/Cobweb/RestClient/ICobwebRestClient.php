<?php
/**
 * This file is part of the "Nette Cobweb Client".
 *
 * Copyright (c) 2014 Ing. Jan Svantner (http://www.janci.net)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace JPSoftware\Cobweb\RestClient;

interface ICobwebRestClient
{
    public function setServerUri($uri = "http://localhost/");

    public function setProjectToken($projectToken = "unknown");

    public function log($message, $state, \DateTime $errorDateTime);

    public function logFile($fileName, $message, $state, \DateTime $errorDateTime);
}
