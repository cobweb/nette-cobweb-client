<?php
/**
 * This file is part of the "Nette Cobweb Client".
 *
 * Copyright (c) 2014 Ing. Jan Svantner (http://www.janci.net)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace JPSoftware\Cobweb\RestClient;

use Guzzle\Http\Client;

class DefaultRestClient implements ICobwebRestClient
{
    const EMPTY_FILE_NAME = "~n.x~";

    private $serverUri = "http://localhost/api";

    private $projectToken = "unknown";

    private $protocols = array(
        'uni' => array(
            'version' => 'info/version'
        ),
        '1.0.0' => array(
            'submit' => 'log/send'
        )
    );

    private $protocolVersion;

    private $restClient;

    public function setServerUri($uri = "http://localhost/api")
    {
        $this->serverUri =  $uri;
    }

    public function setProjectToken($projectToken = "unknown")
    {
        $this->projectToken = $projectToken;
    }

    /**
     * @return Client
     */
    private function getRestClient()
    {
        if (!isset($this->restClient)) {
            $this->restClient = new Client();
        }

        return $this->restClient;
    }

    public function getProtocolVersion()
    {
        if (!isset($this->protocolVersion)) {
            $knownProtocols = implode(',', array_keys($this->protocols));
            $url = $this->serverUri . '/' . $this->protocols['uni']['version'];

            $request = $this->getRestClient()->get($url);
            $request->getQuery()->set('clientProtocols', $knownProtocols);
            $request->getQuery()->set('token', $this->projectToken);
            $rawResponse = $request->send();
            $response = $rawResponse->json();
            if (isset($response['success']) && $response['success'] == true) {
                if (isset($response['communicationProtocol'])) {
                    $this->protocolVersion = $response['communicationProtocol'];
                } else {
                    throw new \Exception("Server and client has different communication protocols.");
                }
            }
        }

        return $this->protocolVersion;
    }

    public function log($message, $state, \DateTime $errorDateTime)
    {
        return $this->logFile(self::EMPTY_FILE_NAME, $message, $state, $errorDateTime);
    }

    public function logFile($fileName, $message, $state, \DateTime $errorDateTime)
    {
        $url = $this->serverUri . '/' . $this->protocols[$this->getProtocolVersion()]['submit'];

        $fileContent = isset($fileName) && $fileName !== self::EMPTY_FILE_NAME && file_exists($fileName) ? @file_get_contents($fileName) : null;

        if ($fileContent === false) {
            $fileContent = null;
        }

        $data = array(
            'message' => $message,
            'level' => $state,
            'created' => $errorDateTime->format("Y-m-d H:i:s"),
            'fileContent' => $fileContent,
            'token' => $this->projectToken
        );

        $rawResponse = $this->getRestClient()->post($url, array(), $data)->send();
        $response = $rawResponse->json();

        if (isset($response["success"]) && $response["success"] == true) {
            return true;
        } else {
            return false;
        }
    }
}
